Demo Project: Run Nexus on a remote DigitalOcean server and Deploy a Java Maven App to Nexus

Project
Run Nexus on a remote DigitalOcean server and Publish a Java Maven App to Nexus

Technologies used:
Nexus, DigitalOcean, Linux, Java, Maven, IntelliJ IDEA

Project Description:
Set up configure a remote Linux Ubuntu server on DO

SSH into the server to establish root and user control

Downloaded a specific java (Open JDK version-8-jre-headless) because our project Repository (Nexus) will be configured with Open JDK version 8-jre-headless)

For security best practice, I created and configured Nexus Linux user, providing required credentials to also SSH into the server

On my local labtop (workstation), where my Java Maven application project is hosted, I ran the following commands to build the pom.xml jar file and to publish the jar files to a configured remote nexus Repository

Commands: 
  mvn package and mvn deploy - to generate /target folders where the java pom.xml files are located

  secure copy (scp) - copied the jar files to the ubuntu server at the root user destination. With the pom.xml files now available in the remote server, I can deploy the files to the configured nexus maven hosted Repository.
  
  java -jar - command was ran to start the application that can be accessed in any UI using our remote server endpoint ip address and designated port. 
